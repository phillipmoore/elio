from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from django.db.models.fields import IntegerField
from django.db.models import Q

from .models import User, ProspectiveUser, Location, Connection, Profile, ProfileResource, Message
from .utils import get_mutual_connection_queryset


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'name', 'last_profile_id')

class ConnectionAcceptedSerializer(serializers.HyperlinkedModelSerializer):
    initiator = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = Connection
        fields = (
            'id', 
            'initiator',
            'accepted', 
        )

class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer(read_only=True)
    blocked_profiles = serializers.PrimaryKeyRelatedField(
        many=True, 
        queryset=Profile.objects.all(), 
        read_only=False
    )

    mutual_count = serializers.SerializerMethodField('get_mutual_count')
    def get_mutual_count(self, instance):
        count = 0
        # is only able to get request via context on GET, which is fine.
        # in order to avoid error on other request types, we need to check for request here
        request = self.context.get('request') 
        if request:
            last_profile_id = request.user.last_profile_id
            count = get_mutual_connection_queryset(instance.pk, last_profile_id).count()
        return count
        
    connection_to_user = serializers.SerializerMethodField('get_connection')
    def get_connection(self, instance):
        # is only able to get request via context on GET, which is fine.
        # in order to avoid error on other request types, we need to check for request here
        request = self.context.get('request') 
        if request:
            last_profile_id = request.user.last_profile_id
            # getting the connection between this profile and logged in user profile
            q1 = Q(initiator=instance.pk) | Q(responder=instance.pk)
            q2 = Q(initiator=last_profile_id) | Q(responder=last_profile_id)
            connection = Connection.objects.filter(q1).filter(q2).first()
        return ConnectionAcceptedSerializer(connection).data if connection else None

    class Meta:
        model = Profile
        fields = (
            'id',
            'user',
            'username', 
            'bio', 
            'avatar', 
            'connection_count', 
            'mutual_count',
            'is_visible',
            'notifications',
            'blocked_profiles',
            'resource_order',
            'connection_to_user',
        )

class ProfileResourceSerializer(serializers.HyperlinkedModelSerializer):
    profile = serializers.PrimaryKeyRelatedField(queryset=Profile.objects.all())
    class Meta:
        model = ProfileResource
        fields = ('id', 'profile', 'resource_type', 'url', 'handle', 'token', 'token_expiration',)
        validators = [
            UniqueTogetherValidator(
                queryset=ProfileResource.objects.exclude(resource_type='wp'),
                fields=['profile', 'resource_type']
            )
        ]

class ProfileAvatarSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Profile
        fields = ('id','avatar')

class ProspectiveUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProspectiveUser
        fields = ('email', 'first_name', 'last_name', 'password', 'uuid')

class MessageSerializer(serializers.HyperlinkedModelSerializer):
    connection = serializers.PrimaryKeyRelatedField(queryset=Connection.objects.all() ,read_only=False)
    class Meta:
        model = Message
        fields = ('id', 'connection', 'sender_id', 'text', 'date_sent', 'was_opened', 'was_liked')

class ConnectionSerializer(serializers.HyperlinkedModelSerializer):
    initiator = ProfileSerializer(read_only=True)
    initiator_id = serializers.IntegerField(write_only=True)
    responder = ProfileSerializer(read_only=True)
    responder_id = serializers.IntegerField(write_only=True)

    last_message = serializers.SerializerMethodField('get_last_message')
    def get_last_message(self, instance):
        # is only able to get request via context on GET, which is fine.
        # in order to avoid error on other request types, we need to check for request here
        request = self.context.get('request') 
        if request:
            # getting last message associated with this connection, excluding logged in user profile
            last_profile_id = request.user.last_profile_id
            message = Message.objects.filter(connection=instance).exclude(sender_id=last_profile_id).last()
        return MessageSerializer(message).data if message else None

    class Meta:
        model = Connection
        fields = (
            'id', 
            'initiator', 
            'initiator_id',
            'responder', 
            'responder_id',
            'accepted', 
            'initiator_hidden', 
            'responder_hidden',
            'last_message',
        )

class LocationSerializer(serializers.HyperlinkedModelSerializer):
    owner = ProfileSerializer(read_only=True)
    class Meta:
        model = Location
        fields = (
            'id', 
            'owner', 
            'latitude', 
            'longitude', 
            'location_time', 
        )

class ValuesListSerializer(serializers.Serializer):
    id_list = serializers.ListField(
       child=serializers.IntegerField()
    )

