from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'prospective_users', views.ProspectiveUserViewSet)
router.register(r'locations', views.LocationViewSet)
router.register(r'userlocation', views.LocationUserViewSet)
router.register(r'connections', views.ConnectionViewSet)
router.register(r'profiles', views.ProfileViewSet)
router.register(r'profile-resources', views.ProfileResourceViewSet)
router.register(r'messages', views.MessageViewSet)
router.register(r'message-connections', views.ConnectionMessageViewSet)
router.register(r'notifications', views.NotificationsViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('confirm_user/<uuid>', views.ConfirmUserView.as_view(), name='confirm_user'),
    path('forgot_password/<email>', views.ForgotPasswordView.as_view(), name='forgot_password'),
    path('reset_password/<uuid>', views.ResetPasswordView.as_view(), name='reset_password'),
    path('get_token/', views.GetTokenView.as_view(), name='get_token'),
    path('image_shrink/', views.ShrinkImageView.as_view(), name='shrink_image'),
]