from PIL import Image
import math
import random
from io import BytesIO
import sys
import requests
import urllib.parse

from django.db.models import Q
from django.apps import apps

def get_mutual_connection_queryset(profileId, app_profileId): # these should both be ids
    Connection = apps.get_model("tokens", "Connection")

    def _get_id_list(values, app_profileId):
        key = 'initiator' if values['initiator'] != app_profileId else 'responder'
        return values[key]

    app_user_connections = Connection.objects.filter(Q(initiator=app_profileId) | Q(responder=app_profileId))
    app_user_values = app_user_connections.values('initiator', 'responder')
    values_array = [_get_id_list(c, app_profileId) for c in app_user_values]
    try:
        key = values_array.index(int(profileId))
        del values_array[key]
    except ValueError:
        pass

    all_connections = Connection.objects.filter(Q(initiator=profileId) | Q(responder=profileId))
    mutual_connections = all_connections.filter(Q(initiator__in=values_array) | Q(responder__in=values_array))

    return mutual_connections

def build_username(instance, digit=False):
    '''makes usernames out of names e.g. Will Smith -> Will_Smith4'''
    Profile = apps.get_model("tokens", "Profile")

    name_text = instance.name.replace(' ', '_')
    username = name_text if not digit else '%s%s' % (name_text, str(digit))
    try:
        Profile.objects.get(username=username)
    except:
        return username
    else:
        use_digit = (digit + 1) if digit else 2
        return build_username(instance, use_digit)

def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.user.id, filename) 

def resize_avatar_max_1600(data):
    basewidth = 1600
    img_format = data.image.format
    img = Image.open(data)
    width, height = img.size
    if width > 1600:
        wpercent = (basewidth/float(width))
        hsize = int((float(height)*float(wpercent)))
        data.image = img.resize((basewidth, hsize), Image.ANTIALIAS)
        temp = BytesIO()
        img.save(temp, format=img_format)
        data.file = temp

    return data

def scramble_and_save_image(self, file):
    '''a method that randomly scrambles an image into tiles'''
    x = 16
    img = Image.open(file)

    width, height = img.size
    tile_width = width/x
    tile_height = height/x

    new_order = list(range((x * x)))
    random.shuffle(new_order)

    collage = Image.new('RGB', (width, height))

    for n in range((x * x)):
        left = tile_width * (n % x)
        top = tile_height * math.floor(n/x)
        right = (tile_width * (n % x)) + tile_width
        bottom = (tile_height * math.floor(n/x)) + tile_height

        the_crop = img.crop((left, top, right, bottom))
        crop_left = math.ceil(tile_width * (new_order[n] % x))
        crop_top = math.ceil(tile_height * math.floor(new_order[n]/x))

        collage.paste(the_crop, (crop_left, crop_top))

    user_folder = 'user_{0}/'.format(self.user.id)
    scrambled_name = '{0}scrambled_{1}'.format(user_folder, self.avatar.name.split(user_folder)[1])
    location_and_name = '{0}/{1}'.format(self.avatar.storage._wrapped.location, scrambled_name)
    # import pdb;pdb.set_trace()
    collage.save(location_and_name)

def resize_image(image_url, base_width):
    safe_image_url = urllib.parse.unquote(image_url)
    safe_base_width = round(float(base_width)) #+ 200 # add a little clarity

    res = requests.get(safe_image_url)
    content_type = res.headers['Content-Type']
    image_format = content_type.replace('image/', '').upper()

    img = Image.open(BytesIO(res.content))
    wpercent = (safe_base_width / float(img.size[0]))
    hsize = round((float(img.size[1]) * float(wpercent)))
    smol_img = img.resize((safe_base_width, hsize), Image.LANCZOS)
    return (smol_img, content_type, image_format)

