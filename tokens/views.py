from uuid import UUID
from geopy import distance

from django.shortcuts import get_object_or_404
from django.views.generic.base import View, TemplateView
from django.http import JsonResponse, HttpResponse, FileResponse
from django.db.models import Q, Case, When, Max

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework.decorators import action
from rest_framework import status
from rest_framework.response import Response

from .serializers import (
    UserSerializer, 
    ProfileAvatarSerializer, 
    ProspectiveUserSerializer, 
    LocationSerializer,
    ConnectionSerializer,
    ProfileSerializer,
    ProfileResourceSerializer,
    MessageSerializer,
    ValuesListSerializer
)
from .models import User, ProspectiveUser, Location, Connection, Profile, ProfileResource, Message
from .utils import get_mutual_connection_queryset, resize_image

# UserSerializer(user).data

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('email')
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]
    parser_classes = (MultiPartParser, JSONParser)

    def get_object(self):
        try:
            # checking if lookup_field is pk
            int(self.kwargs.get(self.lookup_field))
        except ValueError:
            email = self.kwargs.get(self.lookup_field)
            #a frustrating solution to deal with sending periods/dots in emails address strings
            email = email.replace('<dot>', '.')
            object_or_404 = get_object_or_404(User, email=email)
        else:
            pk = self.kwargs.get(self.lookup_field)
            object_or_404 = get_object_or_404(User, pk=pk)

        return object_or_404

class ProspectiveUserViewSet(viewsets.ModelViewSet):
    queryset = ProspectiveUser.objects.all().order_by('email')
    serializer_class = ProspectiveUserSerializer
    lookup_field = 'email'

    def get_object(self):
        ''' a frustrating function to deal with sending periods/dots in emails address strings'''
        email = self.kwargs.get(self.lookup_field)
        email = email.replace('<dot>', '.')
        return get_object_or_404(ProspectiveUser, email=email)

class ConfirmUserView(TemplateView):

    template_name = "confirm_user.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            uuid_obj = UUID(context['uuid'], version=4)
        except ValueError:
            context['message'] = "This is not a valid token. Please <a href='#'>return to the app</a> and try signing up again"
        else: 
            prospective_user = ProspectiveUser.objects.filter(uuid=context['uuid']).first()
            existing_user = None
            if prospective_user:
                existing_user = User.objects.filter(email=prospective_user.email).first()
            if prospective_user and not existing_user:
                new_user = User.objects.create(
                    email=prospective_user.email,
                    first_name=prospective_user.first_name,
                    last_name=prospective_user.last_name,
                    password=prospective_user.password,
                )
                new_user.save()
                prospective_user.delete()
                context['message'] = "Thank you %s %s you can return to the app to log in" % (new_user.first_name, new_user.last_name)
            elif prospective_user and existing_user:
                context['message'] = "It looks like you already have an account with us.  Did you forget your password? You can reset your password <a href='#'>here</a> or <a href='#'>return to the app</a> to log in."
            else:
                context['message'] = "This token has expired. Please <a href='#'>return to the app</a> and try signing up again"
        return context

class ForgotPasswordView(View):
    def get(self, request, *args, **kwargs):
        existing_user = User.objects.filter(email=kwargs['email']).first()
        if existing_user:
            message = "click here to reset your password: http://localhost:8000/reset_password/%s" % existing_user.uuid
            existing_user.email_user('reset password', message, "phil@internestcollective.com")
        return JsonResponse({"sucess": "It has been said"})

class ResetPasswordView(TemplateView):
    template_name = "reset_password.html"

    def post(self, request, *args, **kwargs):
        uuid = kwargs['uuid']
        try:
            uuid_obj = UUID(uuid, version=4)
        except ValueError:
            response = "We cannot find a valid user with matching credentials."
        else:
            response = "Password fields do not match"
            if request._post['password'] == request._post['verified_password']:
                existing_user = User.objects.filter(uuid=uuid).first()
                if existing_user:
                    existing_user.set_password(request._post['password'])
                    existing_user.save()
                    response = "Password was changed.  You can return to the app to log in"
                else:
                    response = "We cannot find a valid user with matching credentials"

        return HttpResponse(response)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class LocationViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

    def get_queryset(self):
        # finding all logged-in-user's profiles and excluding these profiles from results
        user_profiles = Profile.objects.filter(user=self.request.user)
        excluded_profiles = user_profiles.values_list('pk', flat=True)
        queryset = Location.objects.exclude(owner__id__in=excluded_profiles)

        # exclude profiles who have hidden this particular logged in user
        users_profile_id = self.request.user.last_profile_id
        hidden_profiles_list = Profile.objects.get(pk=users_profile_id).profile_set.values_list('pk', flat=True)
        queryset = queryset.exclude(owner__pk__in=hidden_profiles_list)

        # exclude profiles that have hidden from everyone
        queryset = queryset.exclude(owner__is_visible=False)
        
        # get current profile to get current location
        user_location = Location.objects.get(owner=self.request.user.last_profile_id)

        ulat = user_location.latitude
        ulat_range = (ulat + 1, ulat - 1)
        ulon = user_location.longitude
        ulon_range = (ulon + 1, ulon - 1)
        user_position = (ulat, ulon)

        # reducing the queryset to a reasonable distance range before we do a list comprehension
        # otherwise we would be doing list comprehension on all users everywhere in the world
        queryset = queryset.filter(
            latitude__lte=ulat_range[0], 
            latitude__gte=ulat_range[1],
            longitude__lte=ulon_range[0],
            longitude__gte=ulon_range[1]
        )         

        #TODO Filter by location timestamp up here ^

        def _is_within_200_feet(location):
            test_point = (location.latitude, location.longitude)
            distance_away = distance.distance(user_position[:2], test_point[:2]).feet
            print(distance_away)
            return distance_away <= 200

        id_list = [location.owner.id for location in queryset if _is_within_200_feet(location)]
        queryset = queryset.filter(owner__id__in=id_list).order_by('-location_time')

        return queryset

class LocationUserViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    lookup_field = 'owner'

    # def get_queryset(self):
    #     profile = Profile.objects.get(pk=self.request.user.last_profile_id)
    #     return Location.objects.filter(owner=profile) 

class ConnectionViewSet(viewsets.ModelViewSet):
    queryset = Connection.objects.all()
    serializer_class = ConnectionSerializer

    def get_queryset(self, **kwargs):
        profile_id = self.request.GET.get('profile_id')
        mutual = self.request.GET.get('mutual')
        queryset = Connection.objects.all() #.order_by('-connection_date' ) #'accepted' 'initiator_hidden', 'responder_hidden'
        if profile_id and not mutual:
            c1 = Case(
                When(responder=profile_id, then=0), 
                default=1
            )
            c2 = Case(
                When(responder_hidden=False, then=0), 
                default=1
            )
            q1 = Q(initiator=profile_id) | Q(responder=profile_id)
            queryset = queryset.filter(q1).order_by(c2, c1, '-connection_date')
            # q2 = Q(initiator=profile_id) & Q(accepted=False)
            # queryset = queryset.exclude(q2)
        elif profile_id and mutual:
            users_current_profile_id = self.request.user.last_profile_id
            queryset = get_mutual_connection_queryset(profile_id, users_current_profile_id).order_by('-connection_date')
        return queryset

class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    def get_queryset(self, **kwargs):
        user = self.request.GET.get('user')
        queryset = self.queryset.all()
        if user:
            queryset = self.queryset.filter(user=user)
        return queryset

    @action(detail=True, methods=['post', 'put'], name="set_avatar")
    def set_avatar(self, request, pk=None):
        profile = self.get_object()
        serializer = ProfileAvatarSerializer(data=request.FILES)

        if serializer.is_valid():
            profile.set_avatar(serializer.validated_data['avatar'])
            return Response(ProfileSerializer(profile).data)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

class ProfileResourceViewSet(viewsets.ModelViewSet):
    queryset = ProfileResource.objects.all()
    serializer_class = ProfileResourceSerializer

    def get_queryset(self, **kwargs):
        queryset = self.queryset
        profile_id = self.request.GET.get('profile_id')
        profile = Profile.objects.filter(id=profile_id).last()
        if profile:
            queryset = queryset.filter(profile=profile_id)
            if profile.resource_order:
                preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(profile.resource_order)])
                queryset = queryset.order_by(preserved)
        return queryset

    @action(detail=True, methods=['get', 'put', 'patch'], url_path=r"(?P<resource_type>[^/.]+)")
    def get_by_profile_and_type(self, request, pk=None, resource_type=None):
        ''' WARNING: pk == profile.pk in this case. outside of this action  
            (aka. self.get_object() aka. detail),the lookup field will be ProfileResource.pk.
            Furthermore, you need to look for wp (web page) resources by their own pk'''
        first = ProfileResource.objects.filter(profile=pk, resource_type=resource_type).first()
        method = request.META['REQUEST_METHOD']
        if method == 'PUT' or method == 'PATCH':
            if first and request.data:
                for key, val in request.data.items():
                    first.__setattr__(key, val) 
                first.save()             

        return Response(ProfileResourceSerializer(first).data)

class ConnectionMessageViewSet(viewsets.ModelViewSet):
    queryset = Connection.objects.all()
    serializer_class = ConnectionSerializer

    def get_queryset(self, **kwargs):
        qs = Message.objects.filter(
            Q(connection__initiator=self.request.user.last_profile_id) | 
            Q(connection__responder=self.request.user.last_profile_id)
        )
        all_orders = qs.order_by('was_opened', '-date_sent').values_list('connection', flat=True)
        pk_order = (list(dict.fromkeys(all_orders))) # getting unique ids in order
        preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(pk_order)])
        return Connection.objects.filter(id__in=pk_order).order_by(preserved)

class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all().order_by('-date_sent')
    serializer_class = MessageSerializer

    def get_queryset(self, **kwargs):
        connection = self.request.GET.get('connection')
        queryset = self.queryset
        if connection:
            queryset = self.queryset.filter(connection=connection)
        return queryset

    def partial_update(self, request, pk=None):
        ''' if updateing 'was_opened' field, 
            mark all unread messages with same connection 
            as 'was_opened=True' 
        '''
        if request.data.get('was_opened') == True:
            connection = self.get_object().connection
            qs = self.queryset.filter(connection=connection, was_opened=False)
            for query in qs:
                query.was_opened = True
                query.save()

        return super().partial_update(request)


class NotificationsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    def retrieve(self, request, pk=None):
        # locations
        locations = LocationViewSet.get_queryset(self).values_list('id', flat=True)
        locations_dict = {"id_list": locations}

        # messages
        connections_with_messages = ConnectionMessageViewSet.get_queryset(self).values_list('id', flat=True)
        messages = Message.objects.filter(
            connection__in=connections_with_messages, 
            was_opened=False
        ).exclude(
            sender_id=request.user.last_profile_id
        ).values_list('id', flat=True)
        messages_dict = {"id_list": messages}

        #connections
        q1 = Q(accepted=False) & Q(responder_hidden=False)
        connections = Connection.objects.filter(q1, responder=pk) #.exclude(q1)
        connections = connections.values_list('id', flat=True)
        connections_dict = {"id_list": connections}

        return JsonResponse({
            "locations": ValuesListSerializer(locations_dict).data,
            "messages": ValuesListSerializer(messages_dict).data,
            "connections": ValuesListSerializer(connections_dict).data,
        })

class GetTokenView(View):
    def get(self, request, *args, **kwargs):
        # import pdb; pdb.set_trace()
        return JsonResponse({"sucess": "It has been said"})


class ShrinkImageView(View):
    def get(self, request, *args, **kwargs):
        image_url = request.GET.get('url')
        base_width = request.GET.get('base_width')
        (img, content_type, image_format) = resize_image(image_url, base_width)
        response = HttpResponse(content_type=content_type)
        img.save(response, format=image_format, quality=100, subsampling=0)
        return response


