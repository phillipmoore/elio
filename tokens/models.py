from __future__ import unicode_literals

import uuid
from django.db import models
from django.conf import settings
from django.db.models import Q, F
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.hashers import make_password
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.core.validators import RegexValidator

from .managers import UserManager
from .utils import (
    build_username, 
    user_directory_path, 
    resize_avatar_max_1600,
    scramble_and_save_image
)

class Connection(models.Model):
    initiator = models.ForeignKey(
        'tokens.Profile',
        on_delete=models.CASCADE,
        related_name='initiator'
    )
    responder = models.ForeignKey(
        'tokens.Profile',
        on_delete=models.CASCADE,
        related_name='responder'
    )
    accepted = models.BooleanField(default=False)
    initiator_hidden = models.BooleanField(default=False)
    responder_hidden = models.BooleanField(default=False) 
    connection_date = models.DateTimeField(_('date connected'), auto_now_add=True)

    def clean(self):
        symetrical_connection = Connection.objects.filter(initiator=self.responder, responder=self.initiator).first()
        if symetrical_connection:
            raise ValidationError("A symetrical Connection with this Initiator and Responder already exists")
 
        if self.responder == self.initiator:
            raise ValidationError("A User cannot have a Connection to themself")
            
    class Meta:
        unique_together = ('initiator', 'responder')     

    def __str__(self):
        return "%s & %s" % (self.initiator.username, self.responder.username)

class ProspectiveUser(models.Model):
    email = models.EmailField(_('email address'), unique=True)
    name = models.CharField(_('name'), max_length=30, blank=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    password = models.CharField(_('password'), max_length=30, blank=True)
    uuid = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False)

    def set_password(self, raw_password):
        self.password = make_password(raw_password)

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def save(self, *args, **kwargs):
        if not self.pk:
            # import pdb;pdb.set_trace()
            # change password to hashed password prior to save
            self.set_password(self.password)
            # send an email to the user with link to confirm account
            message = "please click here to confirm account: http://localhost:8000/confirm_user/%s" % self.uuid
            self.email_user('please confirm account', message, 'phil@internestcollective.com')
        super(ProspectiveUser, self).save(*args, **kwargs)

class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    name = models.CharField(_('name'), max_length=30, blank=False)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=True)
    is_staff = models.BooleanField(_('staff'), default=False)
    uuid = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False)
    last_profile_id = models.IntegerField(null=True, blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User. TODO: email backend
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)

class Profile(models.Model):
    no_space_validator = RegexValidator(
        r' ',
        _('No spaces allowed'),
        inverse_match=True,
        code='invalid_tag',
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='user'
    )
    avatar = models.ImageField(upload_to=user_directory_path, null=True, blank=True)
    username = models.CharField(
        _('username'), 
        max_length=30, 
        unique=True,
        validators=[no_space_validator]
    )
    bio = models.TextField(_('bio'), max_length=300, blank=True)
    date_added = models.DateTimeField(_('date joined'), auto_now_add=True)
    # safe zones # need to use 'from geopy import distance' for geofencing
    is_visible = models.BooleanField(_('is visible'), default=True)
    notifications = models.BooleanField(_('notifications'), default=False)
    blocked_profiles = models.ManyToManyField('tokens.Profile', blank=True)
    resource_order = models.JSONField(null=True, blank=True)

    @property
    def connection_count(self):
        return Connection.objects.filter(Q(initiator=self) | Q(responder=self)).count()

    def set_avatar(self, data):
        self.avatar = resize_avatar_max_1600(data)
        self.save()

    def save(self, *args, **kwargs):
        is_saving_avatar = not self.avatar._committed
        temp_file = self.avatar._file
        super(Profile, self).save(*args, **kwargs)
        if is_saving_avatar:
            scramble_and_save_image(self, temp_file)

    def __str__(self):
        return ('%s (%s)') % ( self.username, self.user.email )

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile = Profile.objects.create(user=instance, username=build_username(instance))
        # for login purposes, we default to last created Profile
        instance.last_profile_id = profile.id
        instance.save()


# choice names will relate to component names
RESOURCE_CHOICES = [
    ('wp', 'WebPageView'),
    ('ig', 'InstagramView'),
    ('yt', 'YouTubeView'),
]

class ProfileResource(models.Model):
    profile = models.ForeignKey(
        'tokens.Profile',
        on_delete=models.CASCADE,
    )
    resource_type = models.CharField(
        max_length=2,
        choices=RESOURCE_CHOICES,
        default='wp',
    )
    url = models.CharField(_('url'), max_length=200, blank=True)
    handle = models.CharField(_('handle'), max_length=200, blank=True)
    token = models.CharField(_('token'), max_length=500, blank=True)
    token_expiration = models.DateTimeField(_('token expiration'), null=True, blank=True)

    def get_resource_name(self):
        index = [i[0] for i in RESOURCE_CHOICES].index(self.resource_type)
        return RESOURCE_CHOICES[index][1]

    def clean(self):
        first = ProfileResource.objects.filter(profile=self.profile, resource_type=self.resource_type).first()
        # all resources except web pages can only be saved once
        if not self.pk and first and self.resource_type != 'wp':
            raise ValidationError('There is can be only one %s for %s' % (self.get_resource_name(), self.profile))

    def __str__(self):
        return "%s - %s" % (self.profile, self.get_resource_name())

class Location(models.Model):
    owner = models.OneToOneField(
        'tokens.Profile',
        on_delete=models.CASCADE
    )
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    location_time = models.DateTimeField(_('location time'), default=now)

    def __str__(self):
        return "%s's Location" % (self.owner.username)

class Message(models.Model):
    connection = models.ForeignKey(
        Connection,
        on_delete=models.CASCADE,
    )
    sender_id = models.IntegerField(null=True, blank=True)
    text = models.TextField(_('text'), max_length=2000, blank=False)
    date_sent = models.DateTimeField(_('date sent'), auto_now_add=True)
    was_opened = models.BooleanField(_('was opened'), default=False)
    was_liked = models.BooleanField(_('was liked'), default=False)

    def __str__(self):
        return "%s - %s..." % (self.connection, self.text[0:12]) 

