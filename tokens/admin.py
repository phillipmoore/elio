from django.contrib import admin

from .models import User, ProspectiveUser, Location, Connection, Profile, ProfileResource, Message

# Register your models here.

admin.site.register(User)
admin.site.register(Profile)
admin.site.register(ProfileResource)
admin.site.register(ProspectiveUser)
admin.site.register(Location)
admin.site.register(Connection)
admin.site.register(Message)
